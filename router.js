// 1. 动态引入页面文件
const Home = () => import('../views/pages.vue')
// 2. 定义一些路由
// 每个路由都需要映射到一个组件。
// 我们后面再讨论嵌套路由。
const routes = [
  // {
  //   path: '/',
  //   component: Home,
  //   icon: 'el-icon-house',
  //   name: '主页',
  //   children: [
  //     {
  //       path: '/',
  //       component: Dashboard,
  //       icon: 'el-icon-house',
  //       name: '主页',
  //     },
  //     {
  //       path: '/',
  //       component: UserIndex,
  //       icon: 'el-icon-setting',
  //       name: '系统设置',
  //       children: [
  //         { path: 'user', component: User, name: '用户管理' },
  //         { path: 'role', component: Role, name: '角色管理' },
  //       ],
  //     },
  //   ],
  // },
  {
    path: '/',
    component: Home,
    icon: 'el-icon-house',
    name: '首页',
  },
]
// 3. 创建路由实例并传递 `routes` 配置
// 你可以在这里输入更多的配置，但我们在这里
// 暂时保持简单
import { createRouter, createWebHashHistory } from 'vue-router'
const router = createRouter({
  // 4. 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
  history: createWebHashHistory(),
  routes, // `routes: routes` 的缩写
})
// 导出路由
export default router
