import { defineStore } from 'pinia'

// 第一个参数test相当于为id，并且它需要一个唯一名称
export const testStore = defineStore('test', {
  // state相当于data数据
  state: () => {
    return {
      count: 0,
      title: '标题',
      book: {
        id: 1,
        name: 'book1',
        price: 1
      }
    }
  },
  // actions 相当于methods
  actions: {
    increment() {
      this.count++
    }
  },
  // getters 相当于computed
  getters: {
    testCount(state) {
      return state.count + state.title
    }
  }
})