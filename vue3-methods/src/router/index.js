import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView,
  },
  {
    path: '/about',
    name: 'about',
    meta: {
      isShow: false,
    },
    redirect: "/a", // 默认展示第一个路由页面
    component: () => import('../views/AboutView.vue'),
    children: [
      {
        path: '/a',
        name: 'APage',
        meta: {
          isShow: true,
          title: '课程列表',
        },
        component: () => import('../views/aboutComponents/APage.vue'),
      },
      {
        path: '/b',
        name: 'BPage',
        meta: {
          isShow: true,
          title: '讲师列表',
        },
        component: () => import('../views/aboutComponents/BPage.vue'),
      },
      {
        path: '/c',
        name: 'CPage',
        meta: {
          isShow: true,
          title: '个人中心',
        },
        component: () => import('../views/aboutComponents/CPage.vue'),
      },
      {
        path: '/d',
        name: 'DPage',
        meta: {
          isShow: true,
          title: '院校信息',
        },
        component: () => import('../views/aboutComponents/DPage.vue'),
      },
    ],
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes,
})

export default router
