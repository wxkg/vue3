import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import installElementPlus from './plugins/element'
import mitt from 'mitt' // 导入mitt（兄弟组件传值（ 借助mitt ）–全局引入）

const app = createApp(App)

// 全局配置，挂载组件 vue3用的globalProperties, vue2用的prototype
app.config.globalProperties.$mitt = new mitt() // mitt在vue3中挂载到全局
installElementPlus(app)
app.use(store).use(router).mount('#app')