export default {
  namespaced: true, // namespaced属性用于解决不同模块的命名冲突问题
  state: {
    name: '我是info模块',
  },
  getters: {},
  mutations: {
    changeName(state, name) {
      state.name = name
    }
  },
  actions: {},
}
