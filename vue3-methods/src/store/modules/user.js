export default {
  namespaced: true, // namespaced属性用于解决不同模块的命名冲突问题
  state: {
    name: '我是user模块',
    num: 123,
  },
  mutations: {
    addNum(state, num) {
      state.num++
    },
    subNum(state, num) {
      state.num--
    },
    rideNum(state, num) {
      state.num = state.num * num
    },
  },
  actions: {
    changNum: ({ commit,state }, num) => {
      commit('rideNum', num)
    },
  },
  getters: {},
}
