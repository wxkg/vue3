import { createStore } from 'vuex'
import user from './modules/user'
import info from './modules/info'
import vuexAlong from 'vuex-along' // 数据持久化

export default createStore({
  modules: {
    user,
    info
  },
  plugins: [vuexAlong] // 数据持久化
})
