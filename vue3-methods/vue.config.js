const path = require('path')
function resolve (dir) {
  return path.join(__dirname, dir)
}
const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  // 部署开发环境和生产环境
  publicPath: process.env.NODE_ENV === 'production' ? './' : '/',
  // publicPath: '/',  // 基本路径,线上打包的路径
  outputDir: 'dist', // 输出目录
  assetsDir: '', //  用于嵌套生成的静态资产（js，css，img，fonts）的目录。
  productionSourceMap: false, // 生产环境sourceMap,为true的时候打包会生成.map文件,体积大很多 /，打包减小体积,去除.map文件
  // webpack配置
  configureWebpack: {
    devServer: {
      host: 'localhost', // 主机名
      port: 8080, // 端口号
      https: false,
      open: true, // 浏览器自动打开
      proxy: {
        // 配置跨域
        '/api': {
          // 代理1
          target: 'http://jsonplaceholder.typicode.com',
          ws: true, //  如果是http代理此处可以不用设置
          changeOrigin: true, // 如果接口跨域，需要进行这个参数配置
          pathRewrite: {
            '^/api': '',
          },
        }
      },
    },
  },
  chainWebpack: (config) => {
    // 修改文件引入自定义路径
    config.resolve.alias
      .set('@', resolve('src'))
      .set('style', resolve('src/images'))
  },
  // 第三方插件配置
  pluginOptions: {},
})
