module.exports = {
  root: true,
  env: {
    node: true,
    'vue/setup-compiler-macros': true // 解决defineProps报错问题
  },
  'extends': [
    'plugin:vue/vue3-essential',
    'eslint:recommended'
  ],
  parserOptions: {
    parser: '@babel/eslint-parser'
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    "no-unused-vars": 'off' // ‘XXX‘ is defined but never used (no-unused-vars）报错的解决方案
  }
}
